#lang racket
(define (intercala-n n . elem) ; . elem faz com que todos os outros valores paçados sejam em empacotados com uma lista.
  (if (or (< n 1) (null? elem))
      empty
      (cons (first elem)
            (apply intercala-n (- n 1)
                   ;(rotacione elem)
             )
       )
   )
)

#|
(define (rotacione l)
  (coloque-em-ultimo (first l) (rest l))
)|#

(define (mapeia f l)
  (if (null? l)
      empty
      (cons (f (first l)) (mapeia f (rest l)))
   )
)

(mapeia (lambda (x) (* x x x)) '(1 2 3 4 5))