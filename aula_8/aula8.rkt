#lang racket
(define (cria-oper s)
  (cond
    [(eq? s 'soma) (lambda (x y) (+ x y))]
    [(eq? s 'mul) (lambda (x y) (* x y))]
    [else (letrec ([so (lambda (x y) (if (> x y) 0 (+ x (so (+ x 1) y))))]) so)]
  )
)

(define (cria-oper2 op)
  (lambda (x . y)
    (cond
      [(eqv? op 'soma) (+ x y)]
      [(eqv? op 'subtrai) (- x y)]
      [(eqv? op 'mul) (* x y)]
      [else (error "operador não definido")]
      )))

((cria-oper 'soma) 3 4)
((cria-oper 'mul) 3 4)
((cria-oper 'subtrai) 3 4)

;; Solução lista 1
;;q1
(define (concatenar1 l1 l2)
  (if
    (null? l1) l2
    (cons (first l1) (concatenar1 (rest l1) l2))
   )
)
;; q2
(define (concatenar2 l2 l1)
  (if
    (null? l1) l2
    (cons (first l1) (concatenar1 (rest l1) l2))
   )
)

(define (concatenarInv l1 l2) (concatenar1 l2 l1))

(concatenar2 '(1 2 3) '(a b c))
;;q3
(define (concatenar3 l1)
  (cond
    [(null? l1) l1]
    [else (concatenar1 (first l1) (concatenar3 (rest l1)))]
   )
)

;; Escreva um concatenar3 que usa parametros rest ao invés de uma lista de listas
;; função variatica
(define (conctenar4 . ll)
  (if (null? ll)
      ll
      (concatenar1 (first ll) (apply conctenar4 (rest ll)))
   )
)  

#|(define (concatenarV . L)
  (concatenar* L))|#

(concatenar3 '((a b) (c) (d e f)))
(conctenar4 '(a b) '(c) '(d e f))

;; q4
(define (juntar1 l1 l2)
  (cond
    [(null? l1) l2]
    [(null? l2) l1]
    [else
     (cons (first l1)
           (cons (first l2)
                 (juntar1 (rest l1)
                          (rest l2))))]
  )
)
;;q4
(define (juntar l1 l2)
  (if (null? l1)
      l2
      (cons (first l1) (juntar l2 (rest l1)))
  )
)

;; q5
(define (adicionarFinal e l)
  (if (null? l)
      (list e)
      (cons (first l) (adicionarFinal e (rest l)))
   )
)

(adicionarFinal 3 '(a b c d e))

(define (adicionarFinal2 e l)
  (append l (list e)))

(adicionarFinal2 6 '(a b c d e))

;; q7
(define (intercala n e1 e2)
  (if (= n 0)
      empty
      (cons e1 (intercala (- n 1) e2 e1))
   )
)


(intercala 5 'a 'b)
;; q8 de Lucas Natanael
#|
(define (intercala2 N . L)
  (cond
    [(null? L) empty]
    [(eqv? 0 N) empty]
    [else (cons (first L)
                (apply intercala2 (- N 1)
                       (adicionaFinal (first L)
                                      (rest L))))]
    ))|#

;; q8
(define (intercala2 n . l)
  (if (or (<= n 0) (null? l))
      empty
      (cons (first l) (apply intercala2 (- n 1) (adicionarFinal (first l) (rest l))))
   )
)

(intercala2 12 'a 'b 'c 'd 'e 'f)

