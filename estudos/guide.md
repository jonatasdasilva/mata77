# Racket guide 1.0

O **Racket** é uma linguagem de programação. É um dialeto do **Lisp** e um descendente do **Scheme**.

O ambiente do **Racket** é um ambiante de: leitura, avaliação e impressão.

## Primeiro comando do código

No início de todo código a defínição da linguagem é necessária, pois o **DrRacket**, precisa que o idioma apropriado seja escolhido. Sendo assim, o código abaixo deve constar na primeira linha do programa.

```racket
#lang racket
```

## Comentários

Os tipos de comentário mais comuns são linha e comentário de expressão S (usando ";" e "#;" respectivamente).

### De linha única

```racket
;Esse é um comentário 
```

### Bloco de comentários

```racket
#| Esse é um bloco
de comentários
com mais de uma linha |#
```

### Comentários de expressão S

```racket
#;(define (commented-out-function x)
    (print (string-append "A expressão
        S está comentada!")))
```

### Comentários em at-exps

Quando um module usa expreessões at, como:

```racket
#land at-exp racket/base
ou
#lang scribble/manual
```

Você tem acesso aos seguintes tipos de comentários:

```racket
@;{Um bloo de texto,
   o qual vai até o fechamento
   da chave.}
```

Assim como:

```racket
@; Single line text.
```

É importante observaar, que se você estiver usando uma linguagem, a qual usa at-exps, você terá a disposição um desses dois tipos de comentários.

## Identificadores

A sintaxe do Racket para identificadores é especialmente liberal. Excluindo os caracteres especiais: ( ) [ ] { } " , ' \` ; # | \, e exceto para as sequências de caracteres que fazem constantes de número, quase qualquer sequência de caracteres que não sejam de espaço em branco forma um _‹id›_.

## Recuo no código

As quebras de linha e recuo não são significativos para analisar programas **Racket**, mas a maioria dos programadores **Racket** usa um conjunto padrão de converções para tornar o código mais legível. Os identificadores são escritos imediatamente após um parêntese aberto, sem espaço extra, e os parênteses de fechamento nunca ficam em sua própria linha.

DrRacket recua automaticamente de acordo com o estilo padrão quando você digita Enter em um programa ou expressão REPL.

## Definições

Algumas das definições do **Racket** seguem nesta seção.

### Crianco Executável

Salve o arquivo abaixo e selecione **Racket|Create** **Executable** no menu do **Racket**. Isto cria um executável para sua plataforma de interesse.

### S-Expr, Listas e Pares

Nesta seção serão abordados os pontos relevantes sobre as expressões *S*, listas e pares.

#### S-Expr

Expressão simbólica (S-Expr), uma expressão *S* é recursivamente definida como:

* Um **símbolo**, ou
* Um **par da forma (x.y)**, aonde x é uma expressão _S_ e _y_ é uma lista de expressões _S_.

#### Lista ded expressões S - List

Uma **Lista de Expressões S** é definida como:

1. Uma lista **vazia**, ou
2. Um **par na forma (x.y)**, aonde x é uma expressão _S_ e _y_ é uma lista de expressão **S**.

**OBS.:** uma _lista vazia_, não é um _par_ nem uma _S-expr_. Mas, toda _lista_ é um _par_.

#### Par de expressões S - Pair

Um **Par de Expressões S** é definida como:

1. Um **par na forma (x.y)**, aonde _x_ e _y_ são expressões **S**.

**OBS.:** um _par simples (um . par)_, não é uma _S-exp_ nem um _lista_.

## Tipos no Racket

Há cinco tipos de dados básicos que podem aparececr como constantes no código de um programa: números, booleanos, caracteres, strings e símbolos. 

### Números

Documentação: _Todos os números são números complexos. Alguns deles são números reais, e todos os números reais que podem ser representados também são números racionais,exceto +inf.0 (infinito positivo), +inf.f (variante de precisão única, quando ativado via read-single-flonum), -inf.0 (infinito negativo), -inf.f (variante de precisão única, quando habilitada), +nan.0 (não-um-número)e +nan.f (variante de precisão única, quando habilitada). Entre os números racionais, alguns são inteiros,pois rodada aplicada ao número produz o mesmo número._

Um **número** de raquete é exato ou inexato:

* Um **número** _exato_ é qualquer: um inteiro arbitrariamente grande ou pequeno, como _5_, _99999999999999999_, ou _-17_; um racional que é exatamente a rezão de dois inteiros arbitrariamente pequuenos ou grandes, como: _1/2_, _9999999/2_, ou _-3/4_; ou um número complexo com partes reais e imaginárias eatas (onde a parte imaginária não é zero), como _1+2i_ ou _1/2+3/4i_.

* Um **número** _inexato_ é qualquer: uma representação de ponto flutuante IEEE de um número, como _2.0_ ou _3.14e+87_, ou um número complexo  com partes reias e imaginárias que são representações de pontos flutuantes IEEE, como _2.0+3.0i_; como um caso especial, um número complexo inexato pode ter uma parte real zero com uma parte imaginária _inexata_.

Números inexatos impressos com um ponto decimal ou especificador expoente, e números exatos impressos como inteiros e frações. As mesmas convenções se aplicam a constantes de número de leitura, mas _#e_ ou _#i_ podem prefixar um número para forçar sua análise como um número exato ou inexato. Os prefixos _#b_, _#o_, e _#x_ especificam a interpretação binária, octal e hexadecimal de dígitos.

As categorias numéricas inteiros, racionais, reais (sempre racionais) e complexas são definidas da maneira usual, e são reconhecidas pelos procedimentos inteiros?, racionais?, reais?, e complexos? Alguns procedimentos matemáticos aceitam apenas números reais, mas a maioria implementa extensões padrão para números complexos.

### String

**Strings** são sequências de caaracteres representadas, sintaticamente, pelos caracteres que as compõem entre aspas _("hello, wrold")_. Caracteres especiais podem ser indicados utilizando uma sintaxe similar à da _linguagem C_, usando uma sequência de caracteres iniciada por \\.

### Símbolo

Um **Símbolo** é o componente mais primitivo da linguagem. É um valor atômico que é impresso como um _idedntificador_ precedido por **'**. Representados sintaticamente como: _'nome_, onde _nome_ é o nome dado ao símbolo. 

```racket
'eu'   ; é um _símbolo_
'1999  ; **não é um símbolo_
'*abc$ ; é um símbolo

(quote eu) ; outra forma de declarar
(quote *abc$)
```

**OBS.:** um símbolo, não é uma _lista_ nem um _par_.

### Átomos

Um **Átomo** é tudo que não é um _par_. Então, todo símbolo é um _átomo_, assim como: _strings_ e _números_.

As _S-expr_ compõem _símbolos_ no início das _listas_, mas usam _átomos_ nas outras posições.

### Caracteres

**Caracteres** são representados através da sintaxe _#\c_, onde _c_ é um caracter literal ou, no caso de caracteres especiais, o nome do caracter _(#\a, #\Newline)_.

### [Booleanos](https://docs.racket-lang.org/reference/booleans.html)

Os dois valores-verdade da linguagem são: _true_ e _false_, que são represntados por: _#t_ e _#f_. Embora operações que dependem de um valor booleano tipicamente tratem qualquer outra coisa que não _#f_ como verdadeira. O _#t_ valor é sempre _eq?_ para si mesmo, e _#f_ é sempre _eq?_.

## Definição de funções

### Definição de funções Simples

Uma função pode ser definida usando a palavra reservada *define*, da seuinte forma:

```racket
(define (nome-da-função <argumentos>) (<ações>))
; exemplo
(define (g x y) (string-append x y))
```

### Definição de funções com argumentos de palavra-chave

As funções no *Racket* podem ter argumentos de palavra-chave, que são expecificados por uma palavra-chave definida logo após os caracteres: _"#:"_, então os argumentos de palavra-chave tem a seguinte forma: *#:nome-da-keyword nome-da-variável*. Vejamos os exemplos abaixo:

```racket
(define (kinetic-energy #:mass m #:velocity v)
    (* 1/2 m (sqr v)))
```

### Funções Variáticas

São funções que podem receber um número qualquer de parâmetros.

```racket
(* 1 2 3 4 5) ; 1 * 2 * 3 * 4 * 5
```

## Chamadas de função

A chamada de função é dada através de parentezes "(", seguido do nome da funçao e seus parametros e é fechada com ")".

### Chamada de funções Simples

Você pode chamar uma função no Racket envolvendo-a entre parênteses com os argumentos após ela. Isso parece (function argument ...).

```racket
; chamada da função
(g "large" "salmon")
"largesalmon" #;resultado
```

### Chamada de funções com argumentos de palavra-chave

Na chamada das funções com palavra-chave é passado a palavra-chave seguida do valor da mesma.

```racket
(kinetic-energy #:mass 2 #:velocity 1)
```

### Chmada de funções Lambda

As funções Lambda são chamadas da forma como segue abaixo:

```racket
(z 1 2) ;; onde a função definida é: (define z (lambda (n1 n2) (+ n1 n2)))
```

## Condicionais

### if

O próximo tipo mais simples de expressões é uma condicional é o _if_:

```racket
(if <exp> <exp> <exp>)
```

A primenira *<exp>* é a condição de avaliação, se ela produzir um valor diferente de _#f_, então a segunda <exp> é avaliada para o resultado da expressão if inteira, caso contrário, a terceira <exp> é avaliada para o resultado.

```racket
(if (> 2 3)
    "2 é maior que 3"
    "2 é menor que 3")
```

Os ifs podem ser aninhados, mas o *Racket* fornece atalhos mais legíveis por meio dos formulários *and* e *or*.

## and e or

O *and*  forma curto-circuitos, onde ele para e retorna _#f_ quando uma expressão produz _#f_, em caso contrário, ele continua. Já o *or* forma curto-circuitos semelhantes quando encontra um resultado verdadeiro.

```racket
(and <exp>*)
(or <exp>*)

(define (reply-non-string s)
  (if (and (string? s) (string-prefix? s "hello "))
      "hi!"
      "huh?"))
```

Observe que na gramática acima, as formas e e ou funcionam com qualquer número de expressões.

```racket
(define (reply-only-enthusiastic s)
  (if (and (string? s)
           (string-prefix? s "hello ")
           (string-suffix? s "!"))
      "hi!"
      "huh?"))
```

### sequência de testes

Uma forma de abreviar uma sequência de testes é a forma *cond*:

```racket
(cond {[ ‹expr› ‹expr›* ]}*)
```

A forma *cond* contém uma sequência de cláusulas entre colchetes. Em cada cláusula, a primeira *<exp>* é uma expressão de teste. Se produzir true, então as *<exp>* restantes da cláusula são avaliadas, e a última fornece a resposta para toda  a expressão *cond*, o resto das clásulas são ignoradas. Se a primeira *<exp>* produz um _#f_, então as *<exp>* restantes da cláusula são ignoradas e a avaliação continua com a próxima cláusula. A última cláusua pode usar *else* como sinônimo de um _#t_ para a expressão de teste.

O uso de colchetes para cláusulas cond é uma convenção. Em Racket, parênteses e colchetes são realmente intercambiáveis, contanto que( é combinado com ) e [ é combinado com ]. O uso de colchetes em alguns lugares-chave torna o código do Racket ainda mais legível.

### lambda

As funções anônimas são construidas a partir de uma expressão lambda para produzir uma função diretamente. A forma lambda é seguida por indentificadores para os argumentos da função e, em seguida, pelas expressões do corpo da função:

```racket
(lambda (‹id›*) ‹expr›+)
ou
(lambda listaDeParametros S-expr)
```

A avaliação de uma forma lambda por is só produz uma função:

```racket
(Lambda (s) (corda-de acréscimo s "!"))
# <procedure>
```

Até agora nos referimos às definições da forma ( definir ‹ id › ‹ expr › ) como "definições de não função". Esta caracterização é enganosa, porque a ‹ expr › pode ser uma forma lambda , caso em que a definição é equivalente a usar a forma de definição de “função”. Por exemplo, as duas seguintes definições de *louder* são equivalentes:

```racket
(define (louder s)
  (string-append s "!"))
 
(define louder
  (lambda (s)
    (string-append s "!")))
 

> louder
#<procedure:louder>
```

Observe que a expressão para *louder*  no segundo caso é uma função “anônima” escrita com lambda , mas, se possível, o compilador infere um nome, de qualquer maneira, para tornar a impressão e o relatório de erros o mais informativo possível.

## Listas

Uma _lista genérica_ é uma sequência finita de **N>=0** elementos. Que nós podemos escrever como: _L = (E1, E2, ..., En)_, onde _L_ é o nome da lista e _N_ é o comprimento da lista. Cada elemento E_i_ é um **átomo** ou uma outra **lista genérica** (definição recursiva).

No **Racket** as listas estão sempre entre parênteses "(" e ")".

**Obs.:** Se _N=0_, então *L* é dita ser uma lista vazia ou nula.

```racket
(a b c)         ; lista com 3 elementos
()              ; lista vazia, que é uma lista e um átomo
(() () ())      ; uma lista de lista vazia
((a b c) x y z) ; uma lista que detém uma sublista (a b c) como um elemento, onde existem 4 elementos na lista principal
```

## Pares

Um **Par** é uma estrutura de dados que contém dois valores. Que nós podemos escrever como: _P = (Ph . Pt)_, onde _P_ é um **par**, _Ph_ é o elemento da cabeça do **par** e _Pt_ é o elemento da cauda do **par**. Os elementos _Ph_ e _Pt_ podem ser um átomo ou um outro **par** (definição recursiva). Uma vez que o par é criado, ele não pode ser alterado (ou seja, é _imutável_). Um par é feito com **cons**. Um par é impresso como uma lista,mas com um ponto entre os dois valores: _(42 . 43)_.

```racket
(cons 42 43)       ; um par com 2 elementos
(cons + -)         ; um par com 2 procedimentos
(car (cons 42 43)) ; retorna o primeiro valor do par
(cdr (cons 42 43)) ; retorna o segundo elemento do par
```

Observe que muitos pares formam listas, sendo assim o par: _(a . (b .(c . ())))_ é igual a lista: _(a b c)_.

## Operações

### Cabeça

Dada uma lista **L = (E1, E2, En)** ou um par **P = (Ph . Pt)**, então _E1_ é dito ser a cabeça (head) da lista ou par. A operação **car** ou **first** (só usado para listas) detém como retorno o primeiro elemento, no caso do par é retornado por _car_ e no caso da lista é retornado por _first_, e se for uma lista pode ser usado o _car_ também.

```racket
(first '(a b c)) ; retorna a
(car '(a . b))   ; retorna a
```

### Cauda

Dada uma lista ou um par, as operações **cdr** ou **rest** retornam, respectivamente, a cauda de uma **
lista** e a cada de um **par**.

```racket
(rest '(a b c)) ; retorna '(b c)
(cdr '(a . b))   ; retorna b
```

No **Racket**, as operações _car_ e _cdr_ podem ser encadeadas.

```racket
(cadr '(a b c))         ; retorna b
(cddr '((a b c) x y z)) ; retorna (y z)
```

### Concatenação

A operação **cons** retorna um par (ou lista) que é a concatenação de uma cabeça com uma cauda.

```racket
(cons 'a '(a b))   ; retorna (a b c)
(cons 'a '())      ; retorna (a)
(cons '(a) '(b c)) ; retorna ((a) b c)
```

### Matemáticas

#### exp

Retorna o exponencial e elevado a x.

```racket
(exp x)
```

#### log

Retorna o log neperiano de x.

```racket
(log x)
```

#### sin, cos, tan, asin, acos, atan

Retorna o seno, cosseno, tangente, arcoseno, arco tangente, e arco cosseno de x.

```racket
(sin x)
(cos x)
(tan x)
(asin x)
(acos x)
(atan x)
```

#### Trigonométricas

As funções trigonométricas no **Racket** são:

```racket
(max x1 x2...)      ; maior número do grupo
(min x1 x2...)      ; menor número do grupo
(modulo x1 x2)      ; resto inteiro da fração x1/x2 (MOD)
(gcd num1 num2 ...) ; mdc
(lcm num1 num2 ...) ; mmc 
```

#### Igualdade

